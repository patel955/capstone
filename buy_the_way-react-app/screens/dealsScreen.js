
import * as React from 'react';
import { StyleSheet, Text, View, TextInput, FlatList, Image, TouchableOpacity, Button, Linking } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import SignInScreen from './../App'

const numColumns = 2;

class Deals extends React.Component {

    constructor() {
        super();
        this.state = {
            datasource: []
        }
    }

    componentDidMount() {
        fetch('http://localhost:5000/api/products', {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxMyIsImlhdCI6MTYwMjcwNzAwMywiZXhwIjoxNjAzMzExODAzfQ.lwqlQnjkAwyVU2t2JILwjMvvBvHch2aJJkDu6-MI2VsQigpH1d559k5IgbIY2YBpU7MLacHsegscyzrKPxp8Kw',
                'Content-Type': 'application/json'
            }
        }).then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    datasource: responseJson
                })
            }).catch((error) => { console.log(error) })
    }

    formatData = (dataList, numColumns) => {
        const totalRows = Math.floor(dataList.length / numColumns)
        let totalLastRow = dataList.length - (totalRows * numColumns)

        while (totalLastRow !== 0 && totalLastRow !== numColumns) {
            dataList.push({ key: 'blank', empty: true })
            totalLastRow++
        }

        return dataList
    }

    _renderItem = ({ item, index }) => {
        let { itemStyle, itemText, itemInvisible, itemTextHeader } = styles

        if (item.empty) {
            return <View style={[itemStyle, itemInvisible, itemText, itemTextHeader]} />
        }
        return (
            <TouchableOpacity
                style={{ height: '100%', width: '50%' }}
                onPress={() => this.props.navigation.navigate('Details', { item })}
            >
                <View style={itemStyle}>
                    <Text style={itemTextHeader}>{item.companyName}</Text>
                    <Text style={itemText}>{item.offerValidity}</Text>
                    <Image
                        style={styles.imageContainer}
                        source={{
                            uri: item.imageUrl,
                        }}
                    />
                    <Text style={itemText}>Posted by:</Text>
                    <Text>{item.postedBy}</Text>
                </View>
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                <FlatList
                    data={this.formatData(this.state.datasource, numColumns)}
                    renderItem={this._renderItem}
                    keyExtractor={(item, index) => index.toString()}
                    numColumns={numColumns}
                />
            </View>
        );
    }
}

class DealDetail extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {

        const { item } = this.props.route.params

        return (
            <View>
                <Text style={styles.mainHeading}>{item.productName}</Text>
                <Image style={styles.detailImageContainer} source={{ uri: item.imageUrl }} />
                <View style={styles.hr}>
                    <View style={styles.inHr}>
                        <Text style={{ fontWeight: 'bold' }}>Posted By:</Text>
                        <Text>{item.postedBy}</Text>
                    </View>
                    <View style={styles.inHr}>
                        <Text style={{ fontWeight: 'bold' }}>Offer valid till:</Text>
                        <Text>{item.offerValidity}</Text>
                    </View>
                    <View style={styles.inHrSec}>
                    <Button title="Go to website" onPress={ ()=>{ Linking.openURL(item.websiteUrl)}} />
                    </View>
                </View>
                <View style={{ borderTopWidth: 1, borderTopColor: 'black', marginTop: 5 }}>
                    <Text style={styles.subHeading}>Description</Text>
                    <Text>{item.description}</Text>
                </View>
            </View>
        );
    }
}

const Stack = createStackNavigator();

class DealsScreen extends React.Component {
    render() {
        return (
            <Stack.Navigator>
                <Stack.Screen name="Deals" component={Deals} options={{
                    headerShown: true, title: "Buy-The-Way", headerLeft: null,
                    headerRight: () => (
                        <Button
                            onPress={() => this.props.navigation.navigate('SignIn')}
                            title="Sign Out"
                            color="blue"
                        />
                    ),
                    headerLeft: () => (
                        <Button
                            //onPress={() => this.props.navigation.navigate('SignIn')}
                            title="Filter"
                            color="blue"
                        />
                    )
                }} />
                <Stack.Screen name="Details" component={DealDetail} options={{ headerShown: true, headerBackTitle: "Deals" }} />
                <Stack.Screen name="SignIn" component={SignInScreen} options={{ headerShown: false}} />
            </Stack.Navigator>
        );
    }
}

export default DealsScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    itemStyle: {
        backgroundColor: 'white',
        height: 300,
        flex: 1,
        margin: 2,
        paddingLeft: '2%'
    },
    itemTextHeader: {
        color: 'black',
        fontSize: 24,
        fontWeight: 'bold'
    },
    itemtext: {
        color: 'black',
        fontSize: 5
    },
    itemInvisible: {
        backgroundColor: 'transparent'
    },
    imageContainer: {
        marginTop: '10%',
        marginLeft: '8%',
        marginBottom: '10%',
        width: '80%',
        height: '60%'
    },
    imageButton: {
        backgroundColor: 'transparent'
    },
    mainHeading: {
        fontSize: 30,
        fontWeight: 'bold',
        margin: '2%'
    },
    detailImageContainer: {
        margin: '5%',
        width: '90%',
        height: '50%'
    },
    subHeading: {
        fontSize: 25,
        fontWeight: 'bold',
        margin: '2%'
    },
    hr: {
        borderWidth: 1,
        borderTopColor: 'black',
        borderBottomColor: 'black',
        padding: '2%',
        flex: 1,
        flexDirection: 'row',
        height: "100%"
    },
    inHr: {
        width: '32%',
        height: '100%',
        alignItems: 'center'
    },
    inHrSec: {
        width: '36%',
        height: '100%',
        alignItems: 'center'
    }
});

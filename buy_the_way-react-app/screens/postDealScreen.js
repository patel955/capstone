
import * as React from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Button } from 'react-native';


class PostDealScreen extends React.Component {
    render() {
      return (
        <View style={styles.container}>
          <Text>Welcome to post Deals Screen</Text>
        </View>
      );
    }
  }

  export default PostDealScreen;

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#003f5c',
      alignItems: 'center',
      justifyContent: 'center',
    }
  });
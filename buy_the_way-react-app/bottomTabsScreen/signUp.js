import React from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Button } from 'react-native';



export default class SignUp extends React.Component {

    state = {
        email: "",
        password: "",
        username: ""
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.logo}>Buy The Way</Text>

                <View style={styles.inputView} >
                    <TextInput
                        style={styles.inputText}
                        placeholder="Enter Your Username"
                        placeholderTextColor="#003f5c"
                        onChangeText={ (user) => this.setState({ username: user })} />
                </View>
                <View style={styles.inputView} >
                    <TextInput
                        style={styles.inputText}
                        placeholder="Enter Your Email"
                        placeholderTextColor="#003f5c"
                        onChangeText={ (em) => this.setState({ email: em })} />
                </View>
                <View style={styles.inputView} >
                    <TextInput
                        style={styles.inputText}
                        placeholder="Enter Your Password"
                        placeholderTextColor="#003f5c"
                        onChangeText={(pss) => this.setState({ password: pss })} />
                </View>
                <View style={styles.inputView} >
                    <TextInput
                        style={styles.inputText}
                        placeholder="Confirm password"
                        placeholderTextColor="#003f5c" />
                </View>
                <TouchableOpacity>
                    <Text style={styles.forgot}>Policy</Text>
                </TouchableOpacity>
                <Button
                    title="Sign Up"
                    onPress={() =>
                        fetch('http://localhost:5000/api/auth/signup', {
                            method: 'POST',
                            headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json'
                            },
                            body: JSON.stringify(
                                {
                                    username: this.state.username,
                                    email: this.state.email,
                                    name: "qwerty3",
                                    password: this.state.password
                                }
                            )
                        })
                        .then((response) => response.json())
                        .then((responseData) => {
                            console.log("RESULTS HERE:", responseData)})
                            .catch((error) =>{
                                console.error(error);
                              }) 
                    }
                />
            </View>
        );
    }
}




const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#003f5c',
        alignItems: 'center',
        justifyContent: 'center',
    },
    logo: {
        fontWeight: "bold",
        fontSize: (Platform.OS === 'ios') ? 50 : 45,
        color: "#fb5b5a",
        marginBottom: "5%"
    },
    inputView: {
        width: "80%",
        backgroundColor: "#465881",
        borderRadius: 10,
        height: "8%",
        marginBottom: "5%",
        justifyContent: "center",
        padding: 20
    },
    inputText: {
        height: 50,
        color: "white"
    },
    forgot: {
        color: "white",
        fontSize: 15
    },
    signUpBtn: {
        width: "80%",
        backgroundColor: "#fb5b5a",
        borderRadius: 10,
        height: "8%",
        alignItems: "center",
        justifyContent: "center",
        marginTop: "3%",
        marginBottom: "3%"
    },
});


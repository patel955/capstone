/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import * as React from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Button, FlatList, Image, Linking } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import PostDealScreen from './screens/postDealScreen';
import DealsScreen from './screens/dealsScreen';
//import HomeScreen from './bottomTabsScreen/homeScreen';
//import SignUp from './bottomTabsScreen/signUp';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();


class HomeScreen extends React.Component {
  render() {
    return (
      <Tab.Navigator>
        <Tab.Screen name="Deals" component={DealsScreen} />
        <Tab.Screen name="PostDeal" component={PostDealScreen} />
      </Tab.Navigator>
    );
  }
}

class SampleScreen extends React.Component {

  render() {
    return (
      <View style={styles.container}>
        <Button title="Click me" onPress={ ()=>{ Linking.openURL('https://google.com')}} />
      </View>
    );
  }
}

class SignUp extends React.Component {

  state = {
    email: "",
    password: "",
    confirmPassword: "",
    username: "",
    name: "",
    error: ""
  }

  signingup = (email, pass, cpass, username, name) => {
    if (pass == cpass) {
      fetch('http://localhost:5000/api/auth/signup', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(
          {
            username: username,
            email: email,
            name: name,
            password: pass
          }
        )
      })
        .then((response) => {
          if (response.status == 201 && this.state.password == this.state.confirmPassword) {
            this.props.navigation.navigate('Home')
            this.setState({
              email: "",
              password: "",
              confirmPassword: "",
              username: "",
              name: "",
              error: ""
            })
          }
          else if (response.status == 400) {
            if (this.state.email == "" || this.state.password == "" || this.state.confirmPassword == "" || this.state.username == "" || this.state.name == "") {
              this.setState({ error: "Please enter all fields!" })
            }
            else {
              this.setState({ error: "Email is already taken!" })
            }
          }
          else if (response.status == 406) {
            this.setState({ error: "Username already taken!" })
          }
          console.log(response)
        })
        .catch((error) => {
          console.error(error);
        })
    }
    else {
      this.setState({ error: "Password does not match!" })
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.logo}>Buy The Way</Text>

        <View style={styles.inputView} >
          <TextInput
            style={styles.inputText}
            placeholder="Enter Your Name"
            placeholderTextColor="#003f5c"
            onChangeText={(fname) => this.setState({ name: fname })}
            value={this.state.name}
          />
        </View>
        <View style={styles.inputView} >
          <TextInput
            style={styles.inputText}
            placeholder="Enter Your Username"
            placeholderTextColor="#003f5c"
            onChangeText={(user) => this.setState({ username: user })}
            value={this.state.username}
          />
        </View>
        <View style={styles.inputView} >
          <TextInput
            style={styles.inputText}
            placeholder="Enter Your Email"
            placeholderTextColor="#003f5c"
            onChangeText={(em) => this.setState({ email: em })}
            value={this.state.email}
          />
        </View>
        <View style={styles.inputView} >
          <TextInput
            style={styles.inputText}
            placeholder="Enter Your Password"
            placeholderTextColor="#003f5c"
            onChangeText={(pss) => this.setState({ password: pss })}
            value={this.state.password}
          />
        </View>
        <View style={styles.inputView} >
          <TextInput
            style={styles.inputText}
            placeholder="Confirm password"
            placeholderTextColor="#003f5c"
            onChangeText={(cpss) => this.setState({ confirmPassword: cpss })}
            value={this.state.confirmPassword}
          />
        </View>
        <Text style={styles.errorText}>{this.state.error}</Text>
        <TouchableOpacity style={styles.loginBtn} onPress={() =>
          this.signingup(this.state.email, this.state.password, this.state.confirmPassword, this.state.username, this.state.name)
        }>
          <Text style={styles.loginText}>Sign Up</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => this.props.navigation.navigate('SignIn')}>
          <Text style={styles.loginText}>Already have an account? Sign In</Text>
        </TouchableOpacity>
      </View>

    );
  }
}

export class SignInScreen extends React.Component {

  state = {
    usernameOrEmail: "",
    password: "",
    error: ""
  }

  login = (email, pass) => {
    fetch('http://localhost:5000/api/auth/signin', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(
        {
          usernameOrEmail: email,
          password: pass
        }
      )
    })
      .then((response) => {
        if (response.status == 200) {
          //change here sample to Home
          this.props.navigation.navigate('Home')
          this.setState({ usernameOrEmail: "", password: "", error: "" })
        }
        else if (response.status == 400) {
          this.setState({ error: "Please enter both fields!" })
        }
        else if (response.status == 401) {
          this.setState({ error: "Invalid username or password!" })
        }
        console.log(response)
      })
      .catch((error) => { console.log(error) })
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.logo}>Buy The Way</Text>
        <View style={styles.inputView} >
          <TextInput
            style={styles.inputText}
            placeholder="Enter Email Or Username"
            placeholderTextColor="#003f5c"
            autoCapitalize="none"
            onChangeText={(user) => this.setState({ usernameOrEmail: user })}
            value={this.state.usernameOrEmail}
          />
        </View>
        <View style={styles.inputView} >
          <TextInput
            style={styles.inputText}
            placeholder="Enter Password"
            placeholderTextColor="#003f5c"
            autoCapitalize="none"
            onChangeText={(pass) => this.setState({ password: pass })}
            value={this.state.password}
          />
        </View>
        <Text style={styles.errorText}>{this.state.error}</Text>
        <TouchableOpacity style={styles.loginBtn} onPress={() =>
          this.login(this.state.usernameOrEmail, this.state.password)
        }>
          <Text style={styles.loginText}>LOGIN</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => this.props.navigation.navigate('SignUp')}>
          <Text style={styles.loginText}>Not have a account? Signup</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default class App extends React.Component {
  //change here form sample to Home
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator screenOptions={{ headerShown: false }}>
          <Stack.Screen name="SignIn" component={SignInScreen} />
          <Stack.Screen name="SignUp" component={SignUp} />
          <Stack.Screen name="Home" component={HomeScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#003f5c',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    fontWeight: "bold",
    fontSize: 50,
    color: "#fb5b5a",
    marginBottom: 40
  },
  inputView: {
    width: "80%",
    backgroundColor: "#465881",
    borderRadius: 10,
    height: 50,
    marginBottom: 20,
    justifyContent: "center",
    padding: 20
  },
  inputText: {
    height: 50,
    color: "white"
  },
  loginBtn: {
    width: "80%",
    backgroundColor: "#fb5b5a",
    borderRadius: 10,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20,
    marginBottom: 10
  },
  errorText: {
    fontSize: 20,
    color: 'red',
    alignSelf: 'center'
  },
});



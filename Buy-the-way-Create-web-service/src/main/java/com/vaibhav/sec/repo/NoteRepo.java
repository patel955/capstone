package com.vaibhav.sec.repo;

import com.vaibhav.sec.model.Note;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface NoteRepo extends JpaRepository<Note, Long> {

}
